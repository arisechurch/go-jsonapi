package jsonapi

import (
	"encoding/json"
)

func NewDocumentFromModel(
	model Marshalable,
	includeAttrs bool,
) (*Document, error) {
	docId, err := model.GetId()
	if err != nil {
		return nil, err
	}
	relations, err := model.GetRelationships()
	if err != nil {
		return nil, err
	}

	doc := &Document{
		Id:            docId,
		Type:          model.GetType(),
		Relationships: relations,
	}

	if includeAttrs {
		attrs, err := json.Marshal(model)
		if err != nil {
			return nil, err
		}
		jsonAttrs := json.RawMessage(attrs)
		doc.Attributes = &jsonAttrs
	}

	return doc, nil
}
