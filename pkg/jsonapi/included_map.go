package jsonapi

func (m *IncludedMap) Get(typeKey, id string) *Document {
	docMap := map[string]map[string]*Document(*m)
	typeMap := docMap[typeKey]
	if typeMap == nil {
		return &Document{
			Id:   &id,
			Type: typeKey,
		}
	}
	doc, ok := typeMap[id]
	if !ok {
		return &Document{
			Id:   &id,
			Type: typeKey,
		}
	}
	return doc
}

func (m *IncludedMap) Set(key, id string, doc *Document) {
	docMap := map[string]map[string]*Document(*m)
	typeMap := docMap[doc.Type]
	if typeMap == nil {
		docMap[doc.Type] = map[string]*Document{}
		typeMap = docMap[doc.Type]
	}
	docMap[doc.Type][*doc.Id] = doc
}
