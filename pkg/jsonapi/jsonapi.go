package jsonapi

import (
	"encoding/json"
)

type Unmarshalable interface {
	SetId(string) error
	SetRelationships(*Document, *IncludedMap) error
}

type Marshalable interface {
	GetType() string
	GetId() (*string, error)
	GetRelationships() (map[string]*Payload, error)
}

type Payload struct {
	Data     *json.RawMessage `json:"data"`
	Included []*Document      `json:"included,omitempty"`

	Singular bool        `json:"-"`
	Results  []*Document `json:"-"`
	Result   *Document   `json:"-"`

	includedMap *IncludedMap
}

type IncludedMap map[string]map[string]*Document

type Document struct {
	Id            *string             `json:"id"`
	Type          string              `json:"type"`
	Links         map[string]*string  `json:"links,omitempty"`
	Attributes    *json.RawMessage    `json:"attributes,omitempty"`
	Relationships map[string]*Payload `json:"relationships,omitempty"`
}
