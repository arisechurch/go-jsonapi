package jsonapi

import (
	"encoding/json"
	"errors"
	"reflect"
)

var (
	_                 json.Marshaler   = &Payload{}
	_                 json.Unmarshaler = &Payload{}
	unmarshalableType reflect.Type     = reflect.TypeOf((*Unmarshalable)(nil)).Elem()
)

func NewPayloadMany(
	models interface{},
	includeAttrs bool,
) (*Payload, error) {
	p := &Payload{
		Singular: false,
		Results:  []*Document{},
	}

	// Check models is a slice pointer
	mType := reflect.TypeOf(models)
	if mType.Kind() != reflect.Ptr && mType.Elem().Kind() != reflect.Slice {
		return nil, errors.New(
			"jsonapi.NewPayloadMany models param must be a pointer to a slice",
		)
	}

	// Reflect the value
	modelsVal := reflect.Indirect(reflect.ValueOf(models))
	if modelsVal.Len() == 0 {
		return p, nil
	}

	// Get marshalable type with reflect
	marshalable := reflect.TypeOf((*Marshalable)(nil)).Elem()

	// Make assumption that we are a slice of the same type.
	// This could be bad, but for the majority case it is fine.
	if !modelsVal.Index(0).Type().Implements(marshalable) {
		return p, nil
	}

	for i := 0; i < modelsVal.Len(); i++ {
		model := modelsVal.Index(i).Interface().(Marshalable)
		doc, err := NewDocumentFromModel(model, includeAttrs)
		if err != nil {
			return nil, err
		}
		p.Results = append(p.Results, doc)
	}

	return p, nil
}

func NewPayloadOne(
	model Marshalable,
	includeAttrs bool,
) (*Payload, error) {
	doc, err := NewDocumentFromModel(model, includeAttrs)
	if err != nil {
		return nil, err
	}

	return &Payload{
		Singular: true,
		Result:   doc,
	}, nil
}

type jsonPayloadMarshal struct {
	Data     interface{} `json:"data"`
	Included []*Document `json:"included,omitempty"`
}

func (p *Payload) MarshalJSON() ([]byte, error) {
	jsonPayload := &jsonPayloadMarshal{}

	if p.Singular {
		jsonPayload.Data = p.Result
	} else {
		jsonPayload.Data = p.Results
	}

	return json.Marshal(jsonPayload)
}

type jsonPayloadUnmarshal struct {
	Data     *json.RawMessage `json:"data"`
	Included []*Document      `json:"included,omitempty"`
}

func (p *Payload) UnmarshalJSON(data []byte) error {
	jsonPayload := &jsonPayloadUnmarshal{}
	if err := json.Unmarshal(data, jsonPayload); err != nil {
		return err
	}

	p.Data = jsonPayload.Data
	p.Included = jsonPayload.Included

	if p.Data == nil {
		return nil
	}

	dataBytes := []byte(*jsonPayload.Data)
	if string(dataBytes[:1]) == "[" {
		return p.UnmarshalManyDocuments()
	} else if string(dataBytes[:1]) == "{" {
		return p.UnmarshalOneDocument()
	}

	return nil
}

func (p *Payload) UnmarshalOneDocument() error {
	p.Singular = true
	p.Results = []*Document{}
	p.Result = &Document{}

	if err := json.Unmarshal(*p.Data, p.Result); err != nil {
		return err
	}
	if err := p.unmarshalIncluded(); err != nil {
		return err
	}

	return nil
}

func (p *Payload) UnmarshalManyDocuments() error {
	p.Singular = false
	p.Result = nil
	p.Results = []*Document{}

	if err := json.Unmarshal(*p.Data, &p.Results); err != nil {
		return err
	}
	if err := p.unmarshalIncluded(); err != nil {
		return err
	}

	return nil
}

func (p *Payload) unmarshalIncluded() error {
	p.includedMap = &IncludedMap{}
	for _, doc := range p.Included {
		p.includedMap.Set(doc.Type, *doc.Id, doc)
	}
	return nil
}

// Unmarshal the results into models

func (p *Payload) UnmarshalOneModel(model Unmarshalable) error {
	if p.Result == nil {
		return nil
	}

	return p.PopulateModel(p.Result, model)
}

func (p *Payload) UnmarshalManyModels(modelsPtr interface{}) error {
	if len(p.Results) == 0 {
		return nil
	}

	modelsPtrType := reflect.TypeOf(modelsPtr)
	if modelsPtrType.Kind() != reflect.Ptr {
		return errors.New("modelsPtr is not a pointer")
	}
	modelsType := modelsPtrType.Elem()
	if modelsType.Kind() != reflect.Slice {
		return errors.New("modelsPtr is not a slice pointer")
	}
	modelPtrType := modelsType.Elem()
	if modelPtrType.Kind() != reflect.Ptr {
		return errors.New("modelsPtr slice does not contain pointers")
	}
	modelType := modelPtrType.Elem()

	if !modelPtrType.Implements(unmarshalableType) {
		return errors.New("models slice type does not implement Unmarshalable")
	}

	modelsVal := reflect.Indirect(reflect.ValueOf(modelsPtr))
	for _, doc := range p.Results {
		modelPtr := reflect.New(modelType)
		model := modelPtr.Interface().(Unmarshalable)
		if err := p.PopulateModel(doc, model); err != nil {
			return err
		}
		modelsVal.Set(reflect.Append(modelsVal, modelPtr))
	}

	return nil
}

func (p *Payload) PopulateModel(doc *Document, model Unmarshalable) error {
	if doc.Attributes != nil {
		if err := json.Unmarshal(*doc.Attributes, model); err != nil {
			return err
		}
	}
	if err := model.SetId(*doc.Id); err != nil {
		return err
	}
	if err := model.SetRelationships(doc, p.includedMap); err != nil {
		return err
	}

	return nil
}

func (p *Payload) TransformResultsFromMap(docMap *IncludedMap) {
	results := []*Document{}

	for _, idDoc := range p.Results {
		doc := docMap.Get(idDoc.Type, *idDoc.Id)
		if doc == nil {
			continue
		}

		results = append(results, doc)
	}

	p.Results = results
}

func (p *Payload) TransformResultFromMap(docMap *IncludedMap) {
	doc := docMap.Get(p.Result.Type, *p.Result.Id)
	if doc != nil {
		p.Result = doc
	}
}

func (p *Payload) UnmarshalManyRelations(
	docMap *IncludedMap,
	models interface{},
) error {
	if err := p.UnmarshalManyDocuments(); err != nil {
		return nil
	}
	if len(p.Results) == 0 {
		return nil
	}

	p.TransformResultsFromMap(docMap)

	if err := p.UnmarshalManyModels(models); err != nil {
		return err
	}

	return nil
}

func (p *Payload) UnmarshalOneRelation(
	docMap *IncludedMap,
	model Unmarshalable,
) error {
	if err := p.UnmarshalOneDocument(); err != nil {
		return err
	}

	p.TransformResultFromMap(docMap)

	if err := p.UnmarshalOneModel(model); err != nil {
		return err
	}

	return nil
}
