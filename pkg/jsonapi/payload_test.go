package jsonapi_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"encoding/json"
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Marshalable   = &Person{}
	_ jsonapi.Unmarshalable = &Person{}
)

// Define person

type Person struct {
	Id   *string `json:"-"`
	Name string  `json:"name"`

	Friends []*Person `json:"-"`
}

func String(s string) *string {
	return &s
}

func (p *Person) SetId(id string) error {
	p.Id = &id
	return nil
}

func (p *Person) SetRelationships(doc *jsonapi.Document, docMap *jsonapi.IncludedMap) error {
	friends := doc.Relationships["friends"]
	if friends != nil {
		if err := friends.UnmarshalManyRelations(
			docMap,
			&p.Friends,
		); err != nil {
			return err
		}
	}

	return nil
}

func (p *Person) GetType() string {
	return "Person"
}

func (p *Person) GetId() (*string, error) {
	return p.Id, nil
}

func (p *Person) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}

	if p.Friends != nil {
		payload, err := jsonapi.NewPayloadMany(
			&p.Friends,
			false,
		)
		if err != nil {
			return nil, err
		}
		payloads["friends"] = payload
	}

	return payloads, nil
}

// Tests

func TestPayloadUnmarshalJSON_many(t *testing.T) {
	assert := assert.New(t)
	p := &jsonapi.Payload{}

	data := []byte(`{
		"data": [{
			"id": "1",
			"type": "Person",
			"attributes": {
				"name": "One"
			},
			"relationships": {
				"friends": {
					"data": [{
						"id": "2",
						"type": "Person"
					}, {
						"id": "3",
						"type": "Person"
					}]
				}
			}
		}, {
			"id": "4",
			"type": "Person",
			"attributes": {
				"name": "Four"
			},
			"relationships": {
				"friends": {
					"data": [{
						"id": "2",
						"type": "Person"
					}, {
						"id": "3",
						"type": "Person"
					}]
				}
			}
		}],
		"included": [{
			"id": "2",
			"type": "Person",
			"attributes": {
				"name": "Two"
			}
		}, {
			"id": "3",
			"type": "Person",
			"attributes": {
				"name": "Three"
			}
		}]
	}`)
	err := json.Unmarshal(data, p)
	assert.Nil(err)

	people := []*Person{}
	err = p.UnmarshalManyModels(&people)
	assert.Nil(err)

	assert.Equal(2, len(people))
	assert.Equal("Four", people[1].Name)
	assert.Equal("Three", people[1].Friends[1].Name)
}

func TestPayloadMarshalJSON_many(t *testing.T) {
	assert := assert.New(t)

	people := []*Person{{
		Id:   String("1"),
		Name: "One",
		Friends: []*Person{{
			Id:   String("2"),
			Name: "Two",
		}, {
			Id:   String("3"),
			Name: "Three",
		}},
	}, {
		Id:   String("4"),
		Name: "Four",
		Friends: []*Person{{
			Id:   String("2"),
			Name: "Two",
		}, {
			Id:   String("3"),
			Name: "Three",
		}},
	}}

	models := []jsonapi.Marshalable{}
	for _, p := range people {
		models = append(models, p)
	}

	payload, err := jsonapi.NewPayloadMany(&models, true)
	assert.Nil(err)

	data, err := json.Marshal(payload)
	assert.Nil(err)

	assert.Equal(
		`{"data":[{"id":"1","type":"Person","attributes":{"name":"One"},"relationships":{"friends":{"data":[{"id":"2","type":"Person"},{"id":"3","type":"Person"}]}}},{"id":"4","type":"Person","attributes":{"name":"Four"},"relationships":{"friends":{"data":[{"id":"2","type":"Person"},{"id":"3","type":"Person"}]}}}]}`,
		string(data),
	)
}
